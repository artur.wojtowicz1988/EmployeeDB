package EmployeeDB;

import java.util.Scanner;

public class Main {
   private static Scanner scanner = new Scanner(System.in);
    private static Employee employee = new Employee();
    private static EmployeeAddress employeeAddress = new EmployeeAddress();
    private static EmployeePosition employeePosition = new EmployeePosition();
    private static EmployeeSallary employeeSallary = new EmployeeSallary();

    public static void main(String[] args) {
        setName();
        setAddress();
        setPosition();


        System.out.println(employee.getName());
    }

    public static void setName(){
        System.out.println("What is your name?");
        employee.setName(scanner.next());
        System.out.println("What is your surname?");
        employee.setSurname(scanner.next());

    }

    public static void setAddress(){
        System.out.println("What is your street?");
        employeeAddress.setStreet(scanner.next());
        System.out.println("What is your house number?");
        employeeAddress.setHouseNumber(scanner.nextInt());
        System.out.println("What is your flat number?");
        employeeAddress.setFlatNumber(scanner.nextInt());
        System.out.println("What is your city?");
        employeeAddress.setCity(scanner.next());
        System.out.println("What is your zip code?");
        employeeAddress.setZipCode(scanner.next());
    }

    public static void setPosition(){
        System.out.println("What is your position?");
        employeePosition.setPosition(scanner.next());

    }

    public static void setSallary(){
        System.out.println("What is your sallary before trial?");
        employeeSallary.setTrialSallary(scanner.nextInt());
        System.out.println("What is your sallary after trial");
        employeeSallary.setAfterTrialSallary(scanner.nextInt());
    }
}
