package EmployeeDB;

public class EmployeeSallary {

    private int trialSallary;
    private int afterTrialSallary;

//    public EmployeeSallary(int trialSallary, int afterTrialSallary) {
//        this.trialSallary = trialSallary;
//        this.afterTrialSallary = afterTrialSallary;
//    }


    public int getTrialSallary() {
        return trialSallary;
    }

    public void setTrialSallary(int trialSallary) {
        this.trialSallary = trialSallary;
    }

    public int getAfterTrialSallary() {
        return afterTrialSallary;
    }

    public void setAfterTrialSallary(int afterTrialSallary) {
        this.afterTrialSallary = afterTrialSallary;
    }
}
