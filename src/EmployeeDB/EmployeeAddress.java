package EmployeeDB;

public class EmployeeAddress {

//    public EmployeeAddress(String street, int houseNumber, int flatNumber, String city, String zipCode) {
//        this.street = street;
//        this.houseNumber = houseNumber;
//        this.flatNumber = flatNumber;
//        this.city = city;
//        this.zipCode = zipCode;
//    }

    private String street;
    private int houseNumber;
    private int flatNumber;
    private String city;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    private String zipCode;
}
